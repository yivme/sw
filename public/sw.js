var V = '1';
var CACHE_NAME = 'site-cache-' + V;

function parseQString(str) {
	var formatter = function (key, value, accumulator) {
		if (accumulator[key] === undefined) {
			accumulator[key] = value;
			return;
		}

		accumulator[key] = [].concat(accumulator[key], value);
	};
	var ret = Object.create(null);

	if (typeof str !== 'string') {
		return ret;
	}

	var clearStr = str.trim().replace(/^(\?|#|&)/, '');

	if (!clearStr) {
		return clearStr;
	}

	clearStr.split('&').forEach(function (param) {
		var parts = param.replace(/\+/g, ' ').split('=');
		var key = parts.shift();
		var val = parts.length > 0 ? parts.join('=') : undefined;

		val = val === undefined ? null : decodeURIComponent(val);
		formatter(decodeURIComponent(key), val, ret);
	});

	return Object.keys(ret).reduce(function (result, key) {
		var val = ret[key];
		result[key] = val;

		return result;
	}, Object.create(null));
}

self.addEventListener('install', function (event) {
	// установка
	console.log('install');
});

self.addEventListener('activate', function (event) {
	console.log('event activate');
});

self.addEventListener('fetch', function (event) {
	console.log('event fetch');
	event.respondWith(
		caches.match(event.request)
			.then(function (response) {
				// Cache hit - return response
				if (response) {
					console.log('cache:', event.request.url);
					return response;
				}

				// IMPORTANT: Clone the request. A request is a stream and
				// can only be consumed once. Since we are consuming this
				// once by cache and once by the browser for fetch, we need
				// to clone the response.
				var fetchRequest = event.request.clone();
				var getParams = parseQString(event.request.url.split('?')[1]);

				return fetch(fetchRequest).then(
					function (response) {
						console.log('request fetch:', event.request);
						// Check if we received a valid response
						if (!response || response.status !== 200 || response.type !== 'basic') {
							return response;
						}

						// IMPORTANT: Clone the response. A response is a stream
						// and because we want the browser to consume the response
						// as well as the cache consuming the response, we need
						// to clone it so we have two streams.
						var responseToCache = response.clone();

						if (getParams.useCache) {
							console.log('useCache');
							caches.open(CACHE_NAME)
								.then(function (cache) {
									cache.put(event.request, responseToCache);
								});
						}

						return response;
					}
				);
			})
	);
});