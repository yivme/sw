const superagentNo = require('superagent');

function get(requestString) {
	return superagentNo.get(requestString)
		.end((err, res) => {
			// Do something
			console.log(res);
		});
}

module.exports = get;
