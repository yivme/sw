const superagent = require('superagent');
// Require and instantiate a cache module
const cacheModule = require('cache-service-cache-module');
const cache = new cacheModule({storage: 'session', defaultExpiration: 60});

// Require superagent-cache-plugin and pass your cache module
const superagentCache = require('superagent-cache-plugin')(cache);

function get(requestString) {
	return superagent.get(requestString)
		.use(superagentCache)
		.end((err, res) => {
			// Do something
			console.log(res);
		});
}

module.exports = get;
