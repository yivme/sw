
require('whatwg-fetch');
const getCache = require('./req');
const getNoCache = require('./reqNoCache');

fn.addEventListener('click', (e) => {
	e.preventDefault();
	const n = fnn.value;
	const add = (n ? '&n=' + n : '');
	const requestString = '/api/number?useCache=1'.concat(add);


	fnr.innerHTML = requestString;

	fetch(requestString)
		.then((response) => (response.json()))
		.then((payload) => {
			// console.log(payload);
		})
});

const NATIVE_CACHE_NAME = 'NATIVE_CACHE_NAME';

fw.addEventListener('click', (e) => {
	e.preventDefault();
	const n = fww.value;
	const add = (n ? 'n=' + n : '');
	const requestString = '/api/word?'.concat(add);

	fwr.innerHTML = requestString;

	if (global.caches) {
		caches.match(requestString)
			.then((response) => {
				if (response) {
					return response;
				}

				return fetch(requestString)
					.then((response) => {
						if (
							!response
							|| response.status !== 200
							|| response.type !== 'basic'
						) {
							return response;
						}

						// IMPORTANT: Clone the response. A response is a stream
						// and because we want the browser to consume the response
						// as well as the cache consuming the response, we need
						// to clone it so we have two streams.
						var responseToCache = response.clone();

						// if (getParams.useCache) {
						console.log('useCache');
						caches.open(NATIVE_CACHE_NAME)
							.then(function (cache) {
								cache.put(requestString, responseToCache);
							});
						// }

						return response;
					});
			})
			.then((res) => res.json())
			.then((res) => {
				console.log(res);
			});
	}
});

const SUPER_CACHE_NAME = 'SUPER_CACHE_NAME';

fc.addEventListener('click', (e) => {
	e.preventDefault();
	const n = fci.value;
	const add = (n ? 'n=' + n : '');
	const requestString = '/api/color?'.concat(add);

	fcr.innerHTML = requestString;

	getCache(requestString);
});

fetchNoCache.addEventListener('click', (e) => {
	e.preventDefault();
	const n = fci.value;
	const add = (n ? 'n=' + n : '');
	const requestString = '/api/color?'.concat(add);

	fcr.innerHTML = requestString;

	getNoCache(requestString);
});