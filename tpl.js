module.exports = `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="app.css">
</head>
<body>

	<div>
		<div class="fetch">
			<label for="fnn">
				Service worker:
			</label><br/>
			<button id="fn" class="fetch__button">
				Fetch numbers
			</button>
			<input id="fnn" type="text">
			<div id="fnr" class="fetch__request"></div>
		</div>

		<div class="fetch">
			<label for="fww">
				Native fetch:
			</label><br/>
			<button id="fw" class="fetch__button">
				Fetch words
			</button>
			<input id="fww" type="text">
			<div id="fwr" class="fetch__request"></div>
		</div>

		<div class="fetch">
			<label for="fci">
				Superagent fetch:
			</label><br/>
			<button id="fc" class="fetch__button">
				Fetch cache
			</button>
			<input id="fci" type="text">
			<button id="fetchNoCache" class="fetch__button">
				Fetch no cache
			</button>
			<div id="fcr" class="fetch__request"></div>
		</div>
	</div>

	<script src="app.js"></script>
</body>
</html>
`;