const express = require('express');
const tpl = require('./tpl');

const PORT = 8001;
const app = express();
app.use(express.static('public'));

app.get('/api/number', function(req, res, next) {
	const numbersObj = { one: 'one', two: 'two', three: 'three', four: 'four', };

	if (req.query.n) {
		numbersObj.numbers = [];
		for(let i = 0; i < req.query.n; i++) {
			numbersObj.numbers.push(i);
		}
	}

	setTimeout(() => {
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.end(JSON.stringify(numbersObj));
	}, 2000);
});

app.get('/api/word', function(req, res, next) {
	const wordsObj = { apple: 'apple', monkey: 'monkey', mother: 'mother', bus: 'bus', };

	if (req.query.n) {
		wordsObj.words = [];
		for(let i = 0; i < req.query.n; i++) {
			wordsObj.words.push(i);
		}
	}

	setTimeout(() => {
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.end(JSON.stringify(wordsObj));
	}, 2000);
});

app.get('/api/color', function(req, res, next) {
	const colorObj = { blue: 'blue', yellow: 'yellow', green: 'green' };

	if (req.query.n) {
		colorObj.words = [];
		for(let i = 0; i < req.query.n; i++) {
			colorObj.words.push(i);
		}
	}

	setTimeout(() => {
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.end(JSON.stringify(colorObj));
	}, 2000);
});

app.get('/', (req, res, next) => {
	res.send(tpl);
});

app.listen(PORT, () => {
	console.log(`
		Server is listening ${PORT} port
		http://localhost:${PORT}
	`);
});
