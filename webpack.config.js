module.exports = {
	entry: './src/index.js',
	output: {
		path: __dirname + '/public',
		publicPath: '/',
		filename: 'app.js',
	},
	watch: true
};